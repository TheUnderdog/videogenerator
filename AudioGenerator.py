import os
import subprocess
import sys
from FileIO import *

#Linux packages:
#Requires sox be installed
#Requires lame be installed
#Requires libsox-fmt-mp3

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

def TemplateSoxFunction(SourceFilename,OutgoingFilename,NameOfCommand,SingleArgument):
	subprocess.call("sox "+str(SourceFilename)+" "+str(OutgoingFilename)+" "+str(NameOfCommand)+" "+str(SingleArgument));

def Trim(SourceFilename,OutgoingFilename,SecondsIn,SecondsLong):
	subprocess.call("sox "+str(SourceFilename)+" "+str(OutgoingFilename)+" trim "+str(SecondsIn)+" "+str(SecondsLong));

def Append(ListOfFilenames,OutgoingFilename):
	
	CmdStr = "sox "
	
	for Item in ListOfFilenames:
		CmdStr = CmdStr + str(Item)
		
	subprocess.call(CmdStr+" "+str(OutgoingFilename))
	
def Volume(SourceFilename,OutgoingFilename,VolumeMultiplier):
	TemplateSoxFunction(SourceFilename,OutgoingFilename,"vol",VolumeMultiplier)

def Reverse(SourceFilename,OutgoingFilename):
	subprocess.call("sox "+str(SourceFilename)+" "+str(OutgoingFilename)+" reverse");

def Channels(SourceFilename,OutgoingFilename,NumberOfChannels):
	TemplateSoxFunction(SourceFilename,OutgoingFilename,"channels",NumberOfChannels)

def SampleRate(SourceFilename,OutgoingFilename,Rate):
	subprocess.call("sox "+str(SourceFilename)+" -r "+str(Rate)+" channels "+str(OutgoingFilename));

def Pitch(SourceFilename,OutgoingFilename,IncomingValue):
	TemplateSoxFunction(SourceFilename,OutgoingFilename,"pitch",IncomingValue)
