import os
import subprocess
import sys
from FileIO import *

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))
#os.path.join()

G_ValidVideoExt = ["mp4","m4a","flv","webm","mov",'MOV']

def ImageToVideo(ImageFilename,DurationInSeconds,OutputFilename):
	subprocess.call("ffmpeg -loop 1 -i \""+str(ImageFilename)+"\" -c:v libx264 -t \""+str(DurationInSeconds)+"\" -pix_fmt yuv420p -vf \"scale=trunc(iw/2)*2:trunc(ih/2)*2\" \""+str(OutputFilename)+"\"",shell=True)

def CropImage(Filename,CropWidth,CropHeight,CropHorizontal,CropVertical,OverwriteOriginal):
	
	
	CroppedFilename = ""+Filename
	
	if OverwriteOriginal is False:
		CroppedFilename = "cropped_"+Filename
	
	subprocess.call("ffmpeg -i \""+Filename+"\" -vf \"crop="+str(CropWidth)+":"+str(CropHeight)+":"+str(CropHorizontal)+":"+str(CropVertical)+"\" \""+CroppedFilename+"\"")

def CropScreenshot(Filename):
	
	CropImage(Filename,"in_w-2","in_h-2","2","2")

while True:
		
	exit()
